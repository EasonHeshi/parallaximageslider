package es.slothdevelopers.parallaximageslider;

import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.Display;
import android.view.View;

import java.util.Map;

import es.slothdevelopers.parallaximageslider.adapter.ImagePageAdapter;


public class MainActivity extends ActionBarActivity implements ViewPager.OnPageChangeListener {
    int[] pictures = new int[]{
            R.drawable.picture_1,
            R.drawable.picture_2,
            R.drawable.picture_3,
            R.drawable.picture_4,
            R.drawable.picture_5,
            R.drawable.picture_6
    };

    private int width;
    ViewPager viewPager;
    private ImagePageAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViewPagerAndSetAdapter();
        calculateWidth();
    }

    private void initViewPagerAndSetAdapter(){
        viewPager = (ViewPager) findViewById(R.id.parallaxSlider);
        adapter = new ImagePageAdapter(this, pictures);
        viewPager.setAdapter(adapter);

        addPageChangeListenerIfSDKAbove11();
    }

    private void addPageChangeListenerIfSDKAbove11() {
        if (Build.VERSION.SDK_INT >11) {
            viewPager.setOnPageChangeListener(this);
        }
    }

    private void calculateWidth() {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        viewPager.getWidth();

        if (Build.VERSION.SDK_INT <13) {
            width = display.getWidth();
        } else {
            display.getSize(size);
            width = size.x;
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        parallaxImages(position, positionOffsetPixels);
    }



    private void parallaxImages(int position, int positionOffsetPixels) {
        Map<Integer, View> imageViews = adapter.getImageViews();

        for (Map.Entry<Integer, View> entry: imageViews.entrySet()){
            int imagePosition = entry.getKey();
            int correctedPosition = imagePosition - position;
            int displace = -(correctedPosition * width/2)+ (positionOffsetPixels / 2);

            View view = entry.getValue();
            view.setX(displace);
        }
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
